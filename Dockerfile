FROM omjee/baseismicro

MAINTAINER "Om " "ombaghel@gmail.com"

COPY --chown=1724:1724 ./CURRENT_BUILDS/ ${SAG_HOME}/CURRENT_BUILDS/

COPY --chown=1724:1724 ./lib/ ${SAG_HOME}/common/lib/ext/
COPY --chown=1724:1724 ./packages/ ${SAG_HOME}/IntegrationServer/instances/${INSTANCE_NAME}/packages/

COPY --chown=1724:1724 ./projects/ ${SAG_HOME}/IntegrationServer/instances/${INSTANCE_NAME}/packages/WmDeployer/bin/
COPY --chown=1724:1724 ./script/ ${SAG_HOME}/IntegrationServer/instances/${INSTANCE_NAME}/packages/WmDeployer/bin/
COPY --chown=1724:1724 ./repository/ ${SAG_HOME}/IntegrationServer/instances/${INSTANCE_NAME}/packages/WmDeployer/config/repository/

ENTRYPOINT ["/opt/softwareag/IntegrationServer/instances/default/packages/WmDeployer/bin/entrypoint.sh"]
